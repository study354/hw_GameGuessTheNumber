﻿using GuessTheNumber.Data;
using GuessTheNumber.Models;
using GuessTheNumber.Services.Interfaces;

namespace GuessTheNumber.Services.Implementation
{
    internal class GameProcess : IGameProcess
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ISetupGame _setupGame;
        private readonly List<string> _yesAnswer = new() { "y", "yes", };


        private Game _game;
        private readonly Random _random = new();

        public GameProcess(ApplicationDbContext dbContext, ISetupGame setupGame)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _setupGame = setupGame;
        }
       

        private int RunGame()
        {
            int gameScore = _game.Settings.NumberOfTries;

            _game.TargetNumber = _random.Next(_game.Settings.MinRange, _game.Settings.MaxRange);
            for (int tried = 1; tried <= _game.Settings.NumberOfTries; tried++)
            {
                Console.WriteLine($"{Environment.NewLine}Attempt number {tried}. Input number:");
                if(_setupGame.IsWin(_game, Console.ReadLine()))
                {
                    return gameScore;
                }
                gameScore--;
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("The attempts are over.You have lost.");
            Console.ResetColor();
            return 0;
        }

        



        public async Task Start()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Start Game.");
            Console.ResetColor();

            _game = new ();
            _setupGame.SetPlayer(_game);
            _setupGame.SetSettings(_game);

            do
            {
                _game.GameScore = RunGame();
                Console.WriteLine($"You get {_game.GameScore} score.");
                _game.EndGame = DateTime.Now;
                await _dbContext.Games.AddAsync(_game);
                await _dbContext.SaveChangesAsync();

                Console.WriteLine($"{Environment.NewLine}Are we playing again? y/n");
                if (!_yesAnswer.Contains(Console.ReadLine()))
                {
                    Console.WriteLine("End game.");
                    break;
                }
                
                _game = new () { Player = _game.Player, Settings=_game.Settings };
            }
            while (true);

            
        }
    }
}
