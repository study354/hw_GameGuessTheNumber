﻿using GuessTheNumber.Data;
using GuessTheNumber.Models;
using GuessTheNumber.Services.Interfaces;

namespace GuessTheNumber.Services.Implementation
{
    internal class SetupGame : ISetupGame
    {

        private readonly int _numberOfTries = 10;
        private readonly int _maxRange = 100;
        private readonly int _minRange = 1;
        private readonly List<string> _yesAnswer = new() { "y", "yes", };

        private readonly ApplicationDbContext _dbContext;

        public SetupGame(ApplicationDbContext dbContext) => _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));

        public virtual void SetPlayer(Game game)
        {
            string? namePlayer;
            do
            {
                Console.WriteLine("Input Name:");
                namePlayer = Console.ReadLine();
                if (string.IsNullOrEmpty(namePlayer))
                {
                    Console.WriteLine("Incorrect values were entered");
                    continue;
                }
                break;
            }
            while (true);
            Player? player = _dbContext.Players.FirstOrDefault(p => p.NickName == namePlayer);
            if (player == null)
            {
                player = new Player { NickName = namePlayer };
            }
            game.Player = player;
        }

        public virtual void SetSettings(Game game)
        {
            Console.WriteLine($"Default Settings numberOfTries={_numberOfTries}, maxRange={_maxRange}, minRange={_minRange}.{Environment.NewLine}Input new settings? y/n");
            string? answer = Console.ReadLine();
            if (_yesAnswer.Contains(answer?.ToLower()))
            {
                Console.WriteLine("Enter, separated by a space: the number of attempts maximum and minimum values of the number.");
                string[]? input = Console.ReadLine()?.Split(' ');
                if (input != null && input.Length == 3 &&
                    int.TryParse(input[0], out int numberOfTries) &&
                    int.TryParse(input[1], out int maxRange) &&
                    int.TryParse(input[2], out int minRange) &&
                    maxRange > minRange
                    )
                {
                    Console.WriteLine($"Settings settup.");
                    game.Settings = new Settings { Game = game, NumberOfTries = numberOfTries, MaxRange = maxRange, MinRange = minRange };
                    return;
                }
                Console.WriteLine("Incorrect values were entered");
            }
            Console.WriteLine($"Use default Settings.");
            game.Settings = new Settings { Game = game, NumberOfTries = _numberOfTries, MaxRange = _maxRange, MinRange = _minRange };
        }

        public virtual bool IsWin(Game game, string? input)
        {
            if (!int.TryParse(input ?? string.Empty, out int number))
            {
                Console.WriteLine("Error. Input not number!");
            }
            else if (number < game.TargetNumber)
            {
                Console.WriteLine($"Target number is greater than {number}.");
            }
            else if (number > game.TargetNumber)
            {
                Console.WriteLine($"Target number is smaller  than {number}.");
            }
            else if (game.TargetNumber == number)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Congratulations you win!! Target number is {game.TargetNumber}.");
                Console.ResetColor();
                return true;
            }
            return false;

        }
    }
}
