﻿using GuessTheNumber.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessTheNumber.Services.Interfaces
{
    internal interface ISetupPlayer
    {
        public void SetPlayer(Game game);
    }
}
