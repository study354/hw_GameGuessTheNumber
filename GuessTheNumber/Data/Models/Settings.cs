﻿namespace GuessTheNumber.Models
{
    internal class Settings
    {
        public int Id { get; set; }
        public Game Game { get; set; }
        public int GameId { get; set; }
        public int NumberOfTries { get; set; }
        public int MaxRange { get; set; }
        public int MinRange { get; set; }
    }
}
