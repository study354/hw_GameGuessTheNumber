﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessTheNumber.Models
{
    internal class Player
    {
        public int Id { get; set; }
        public string? NickName { get; set; }

    }
}
