﻿namespace GuessTheNumber.Models
{
    internal class Game
    {
        public int Id { get; set; }
        public DateTime StartGame { get; set; }
        public DateTime? EndGame { get; set; }
        public int TargetNumber { get; set; }
        public double GameScore { get; set; } = 0;
        public Player? Player { get; set; }
        public Settings? Settings { get; set; }

    }
}
