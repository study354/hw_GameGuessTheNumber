﻿using GuessTheNumber.Models;
using Microsoft.EntityFrameworkCore;

namespace GuessTheNumber.Data
{
    internal class ApplicationDbContext : DbContext
    {
        private readonly string _connectingString = Environment.GetEnvironmentVariable("ConnectingString") ?? "Data Source=Test.db";

        public DbSet<Game> Games { get; set; } = null!;
        public DbSet<Player> Players { get; set; } = null!;
        public DbSet<Settings> Settings { get; set; } = null!;

        public ApplicationDbContext() => Database.EnsureCreated();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(_connectingString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Game>()
                .Property(g => g.StartGame)
                .HasDefaultValue(DateTime.Now);
        }
    }
}
