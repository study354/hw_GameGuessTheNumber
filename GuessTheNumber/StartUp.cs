﻿using GuessTheNumber.Data;
using GuessTheNumber.Services.Implementation;
using GuessTheNumber.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace GuessTheNumber
{
    internal class StartUp
    {
        public static IHostBuilder CreateHostBuilder(string[] args) => Host.CreateDefaultBuilder(args)
                .ConfigureServices((_, services) =>
                    services.AddDbContext<ApplicationDbContext>()
                            .AddScoped<ISetupGame, SetupGame>()
                            .AddScoped<IGameProcess, GameProcess>());


        public static async Task RunGameAsync(IServiceProvider services)
        {
            using IServiceScope serviceScope = services.CreateScope();
            IServiceProvider provider = serviceScope.ServiceProvider;

            IGameProcess? gameProcess = provider.GetService<IGameProcess>();
            if (gameProcess != null)
            {
                await gameProcess.Start();
                Environment.Exit(0);
            }
        }
    }
}
