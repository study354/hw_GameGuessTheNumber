﻿using GuessTheNumber;
using Microsoft.Extensions.Hosting;


using IHost host = StartUp.CreateHostBuilder(args).Build();

await StartUp.RunGameAsync(host.Services);

await host.RunAsync();
